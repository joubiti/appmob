package com.myinwi.ma.app.data.net

import com.myinwi.ma.app.common.Constants
import com.myinwi.ma.app.data.models.User
import retrofit2.http.Body
import retrofit2.http.POST

interface MyInwiService {

    @POST(Constants.Ws_login)
    suspend fun login(@Body userLogin: User): User
}
