package com.myinwi.ma.app.di

import android.app.Activity
import androidx.fragment.app.Fragment
import com.myinwi.ma.app.MyInwiApplication

/**
 * Helper extension properties for fragments and activities
 * to facilitate access to dependencies in the dagger graph.
 */

val Activity.injector
    get() = (application as MyInwiApplication).component

val Fragment.injector
    get() = (requireActivity().application as MyInwiApplication).component
