package com.myinwi.ma.app.data.net

import com.google.gson.annotations.SerializedName

data class ServerErrorRS(
    @SerializedName("status")
    val code: String?,
    @SerializedName("detail")
    val description: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("errorKey")
    val errorKey: String?

)
