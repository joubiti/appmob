package com.myinwi.ma.app.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.myinwi.ma.app.common.BaseViewModel
import com.myinwi.ma.app.common.Resource
import com.myinwi.ma.app.common.asResource
import com.myinwi.ma.app.data.models.User
import com.myinwi.ma.app.data.repositories.UserRepositories

class LoginViewModel @javax.inject.Inject constructor(private val userAccountRepo: UserRepositories) : BaseViewModel() {

    private val _responseLogin = MutableLiveData<Resource<User>?>()
    val responseLogin: LiveData<Resource<User>?> = _responseLogin

    fun login(userName: String, password: String) = uiCoroutine {
        val loginData = User(
            userName,
            password,
            "1123"
        )
        _responseLogin.value = Resource.Loading // Notify fragments that we are Loading users
        val resultLogin = userAccountRepo.login(loginData)
        _responseLogin.value = resultLogin.asResource() // Map result to resource and notify fragment
    }
}
