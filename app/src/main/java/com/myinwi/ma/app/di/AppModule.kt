package com.myinwi.ma.app.di
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.myinwi.ma.app.common.Constants
import com.myinwi.ma.app.data.net.MyInwiService
import com.myinwi.ma.app.data.net.customFactory
import dagger.Lazy
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn
class AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(client: Lazy<OkHttpClient>, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .customFactory { client.get().newCall(it) } // Initialize Okhttp lazily
            .build()
    }

    @Singleton
    @Provides
    fun providesOkHttp(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
            .connectTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
            .writeTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
            .readTimeout((3 * 60).toLong(), TimeUnit.SECONDS)
            .addInterceptor(logging)

        // .addInterceptor(ChuckInterceptor(context))

        return builder.build()
    }

    @Singleton
    @Provides
    fun providesGson(): Gson = GsonBuilder().setLenient().create()

    @Provides
    @Singleton
    fun provideLeenaService(retrofit: Retrofit): MyInwiService {
        return retrofit.create(MyInwiService::class.java)
    }
}
