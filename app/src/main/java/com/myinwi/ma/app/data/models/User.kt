package com.myinwi.ma.app.data.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("username")
    var username: String?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("deviceToken")
    val deviceToken: String?

)
