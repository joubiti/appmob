package com.myinwi.ma.app.di
import android.content.Context
import com.myinwi.ma.app.ui.LoginViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.hilt.InstallIn
import javax.inject.Singleton

@InstallIn
@Singleton
@Component(
    modules = [
        AppModule::class
    ]
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }

    val loginViewModel: LoginViewModel
}
