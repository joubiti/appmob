package com.myinwi.ma.app.common

sealed class Resource<out T> {

    object Loading : Resource<Nothing>()

    data class Success<out T>(val data: T) : Resource<T>()

    data class Failure(val message: AppMessage, val detail: ErrorDetail) : Resource<Nothing>()
}
