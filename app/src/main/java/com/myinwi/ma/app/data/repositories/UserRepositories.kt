package com.myinwi.ma.app.data.repositories

import com.myinwi.ma.app.common.Result
import com.myinwi.ma.app.data.models.User
import com.myinwi.ma.app.data.net.MyInwiService
import com.myinwi.ma.app.data.net.RetrofitRunner
import javax.inject.Inject

class UserRepositories @Inject constructor(
    private val retrofitRunner: RetrofitRunner,
    private val myInwiService: MyInwiService
) {

    suspend fun login(userLogin: User): Result<User> {
        return retrofitRunner.executeNetworkCall {
            myInwiService.login(userLogin)
        }
    }
}
