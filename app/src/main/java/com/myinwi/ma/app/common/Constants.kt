package com.myinwi.ma.app.common

import com.myinwi.ma.app.BuildConfig

object Constants {
    const val BASE_URL = BuildConfig.BASE_URL

    const val Ws_login = "/authenticate"
}
