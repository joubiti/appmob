package com.myinwi.ma.app

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.myinwi.ma.app.di.AppComponent
import com.myinwi.ma.app.di.DaggerAppComponent
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyInwiApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.factory().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }
    companion object {
        lateinit var INSTANCE: MyInwiApplication
            private set
    }
}
