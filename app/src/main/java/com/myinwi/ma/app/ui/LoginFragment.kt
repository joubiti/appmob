package com.myinwi.ma.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.myinwi.ma.app.R
import com.myinwi.ma.app.common.Resource
import com.myinwi.ma.app.di.injector
import com.myinwi.ma.app.util.viewModel
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {
    private val loginViewModel by viewModel { injector.loginViewModel }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_first.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("params", "value")
            it.findNavController().navigate(R.id.loginFragment, bundle)

            loginViewModel.login("username", "password")
            loginViewModel.responseLogin.observe(
                viewLifecycleOwner,
                { resource ->
                    when (resource) {
                        Resource.Loading -> {
                            print("Loading")
                        }
                        is Resource.Success -> {
                            print("Success")
                        }
                        is Resource.Failure -> {
                            print("Failure")
                        }
                    }

                    println("Resource: $resource")
                }
            )
        }
    }
}
