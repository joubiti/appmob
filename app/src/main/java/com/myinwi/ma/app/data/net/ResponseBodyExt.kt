package com.myinwi.ma.app.data.net

import okhttp3.ResponseBody

fun ResponseBody.stringOrEmpty(): String {
    return try {
        this.string()
    } catch (e: Exception) {
        ""
    }
}
